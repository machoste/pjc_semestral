#!bin/bash
make
echo "------------------------Serial"
./knapsack -so -if largeInput1.txt
./knapsack -so -if largeInput2.txt
./knapsack -so -if largeInput3.txt
./knapsack -so -if largeInput4.txt
echo "------------------------2 Threads"
./knapsack -so -if largeInput1.txt -t 2
./knapsack -so -if largeInput2.txt -t 2
./knapsack -so -if largeInput3.txt -t 2
./knapsack -so -if largeInput4.txt -t 2
echo "------------------------4 Threads"
./knapsack -so -if largeInput1.txt -t 4
./knapsack -so -if largeInput2.txt -t 4
./knapsack -so -if largeInput3.txt -t 4
./knapsack -so -if largeInput4.txt -t 4
echo "------------------------8 Threads"
./knapsack -so -if largeInput1.txt -t 8
./knapsack -so -if largeInput2.txt -t 8
./knapsack -so -if largeInput3.txt -t 8
./knapsack -so -if largeInput4.txt -t 8
echo "------------------------24 Threads"
./knapsack -so -if largeInput1.txt -t 24
./knapsack -so -if largeInput2.txt -t 24
./knapsack -so -if largeInput3.txt -t 24
./knapsack -so -if largeInput4.txt -t 24