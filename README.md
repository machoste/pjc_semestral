# PJC_semestral

Semestrální práce je uložena na https://gitlab.fel.cvut.cz/machoste/pjc_semestral
Za odevzdání se považuje poslední commit.

## Zadání
Zadáním semestrální práce je vytvořit program, který dokáže vypočíst algoritmus knapsack.

## Implementace

Níže jsou popsané jednotlivé třídy a co dělají.
 
### ArgumentParser
Zpracovává argumenty a reaguje na ně. Při správném zadání parametrů vytvoří problém, který poté pošle dále do solveru
Argumenty mohou být typ výstupu, počet vláken, popřípadě typ vstupu a popřípadně soubory na vstup/výstup.

#### Parametry

|Parametr| Použití|   
|---|---|
|-t <počet vláken> | Počet vláken, na kterých se spustí algoritmus|
|-if <vstupní soubor>| Parametr, který indikuje vstup ze souboru|
|-si| Parametr, který indikuje standradní vstup|
|-of <vstupní soubor>| Parametr, který indikuje výstup do souboru|
|-so| Parametr, který indikuje standardní výstup|  

### Solver
* Zpracovává data a vypočítává řešení
* Měří rychlost běhu
* Realizuje vícevláknový běh

### main
* Spouští aplikaci podle daných argumentů
* Propojuje Solver a ArgumentParser

## Generování dat

Pro práci byl použit náhodný generátor, kdy počet věcí je 10'000 a velikost batohu 100'000. Velikost a cena předmětů byly náhodně v rozmezí (0-12000> 

## Výsledky měření

Měření bylo provedeno proti commitu 8a64722f. Výsledky jsou v tests.txt. Testy se dají provést skriptem testScript.sh
|Parametry| Výsledek [ms]|
|---|---|
|largeInput1.txt serial|6352|
|largeInput2.txt serial|6946|
|largeInput3.txt serial|6276|
|largeInput4.txt serial|6343|
|largeInput1.txt -t 2|4308|
|largeInput2.txt -t 2|4249|
|largeInput3.txt -t 2|4253|
|largeInput4.txt -t 2|4275|
|largeInput1.txt -t 4|3984|
|largeInput2.txt -t 4|4013|
|largeInput3.txt -t 4|3996|
|largeInput4.txt -t 4|4017|
|largeInput1.txt -t 8|5345|
|largeInput2.txt -t 8|5388|
|largeInput3.txt -t 8|5346|
|largeInput4.txt -t 8|5392|
|largeInput1.txt -t 24|12240|
|largeInput2.txt -t 24|12523|
|largeInput3.txt -t 24|12324|
|largeInput4.txt -t 24|12339|

Jak je vidět z výsledků, náš paralelizmus přes condition_variable je sice zrychlením, nicméně Knapsack není ideální na paralelizaci, tudíž je zlepšení pouze malé.

## Prostředí

Výsledky měření jsou získané z běhu na WSL na Ubuntu 20.20 na procesoru Amd Ryzen 3900x. 
