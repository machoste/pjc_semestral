#ifndef SEM_PARSE
#define SEM_PARSE

#include "Exceptions.h"
#include <string>
#include <iostream>
#include <sstream>
#include "Solver.h"
#include <fstream>
#include <memory>


class ArgumentParser{
    int threadCount = -1;
    bool standardInput = false;
    bool standardOutput = false;
    std::string outputFileName = "";
    std::string inputFileName = "";
    bool isHelp(std::string const &arg);
    bool isThreadFlag(std::string const &arg);
    bool isStandardInputFlag(std::string const &arg);
    bool isInputFileFlag(std::string const &arg);
    bool isOutputFileFlag(std::string const &arg);
    bool isStandardOutputFlag(std::string const &arg);
    void printHelp();
protected:

public:
    problem getProblem();
    ArgumentParser(int argc, char **argv);
    int getThreadCount();
    std::string getOutputFile();
};

#endif