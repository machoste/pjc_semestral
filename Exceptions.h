#ifndef SEM_EXCEPTIONS
#define SEM_EXCEPTIONS

#include <exception>
#include <iostream>

//--- Exceptions
class my_exception : public std::exception {
public:
    my_exception(std::string s) : text(std::move(s)) {}
    virtual const char* what() const noexcept override {
        return text.c_str();
    }
protected:
    std::string text;
};

class argument_exception : public my_exception {
public:
    using my_exception::my_exception;
};
class help_exception : public my_exception{
public:
    using my_exception::my_exception;
};
class file_exception : public my_exception{
public:
    using my_exception::my_exception;
};

#endif