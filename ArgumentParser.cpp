#include "ArgumentParser.h"

bool ArgumentParser::isHelp(std::string const &arg){
    return arg == "-h" || arg == "--help";
}
bool ArgumentParser::isThreadFlag(std::string const &arg){
    return arg == "-t" || arg == "-T";
}
bool ArgumentParser::isStandardInputFlag(std::string const &arg){
    return arg == "-si" || arg == "-SI";
}
bool ArgumentParser::isInputFileFlag(std::string const &arg){
    return arg == "-if" || arg == "-IF";
}
bool ArgumentParser::isOutputFileFlag(std::string const &arg){
    return arg == "-of" || arg == "-OF";
}
bool ArgumentParser::isStandardOutputFlag(std::string const &arg){
    return arg == "-so" || arg == "-SO";
}

void ArgumentParser::printHelp(){
    std::cout << "Knapsack problem solver" <<std::endl;
    std::cout << "Arguments:" <<std::endl;
    std::cout << "-t <thread_count> {number of threads}" <<std::endl;
    std::cout << "-if <file> {input file}" <<std::endl;
    std::cout << "-si {standard input flag}" <<std::endl;
    std::cout << "-of <file> {output file}" <<std::endl;
    std::cout << "-so {output in cout}" <<std::endl;
    std::cout << "Input for the algorithm is in the following manner:" <<std::endl;
    std::cout << "<Item Count>  <Backpack size> " << std::endl;
    std::cout << "Item Count * <Item Weight>" << std::endl;
    std::cout << "Item Count * <Item Costs>" << std::endl;
    std::cout << "Output for the algorithm is in the following manner:" <<std::endl;
    std::cout << "<Optimal value> " << std::endl;
    std::cout << "<List of taken items> (starting with 1)" << std::endl;
}
int ArgumentParser::getThreadCount(){
    return threadCount;
}

std::string ArgumentParser::getOutputFile(){
    return outputFileName;
}

ArgumentParser::ArgumentParser(int argc, char**argv){
    if(argc == 1) throw argument_exception("! No flags specified, please use -h for help!");
    for(int i = 1; i < argc; i++){
        if(argc == 2 and isHelp(argv[i])){
            printHelp();
            throw help_exception("");
        }
        if(isThreadFlag(argv[i])){
            if(++i < argc && std::stoi(argv[i]) > 0){
                if(threadCount != -1) throw argument_exception("! Multiple thread arguments!");
                threadCount = std::stoi(argv[i]);
                if(threadCount < 0) throw argument_exception("! Cannot run with non-positive number of threads!");
            }
            else
                throw argument_exception("! Thread flag problem!");
            continue;
        }
        if(isStandardInputFlag(argv[i])){
            if(standardInput || inputFileName != "") throw argument_exception("! Multiple input arguments!");
            standardInput = true;
            continue;
        }
        if(isInputFileFlag(argv[i])){
            if(standardInput || inputFileName != "") throw argument_exception("! Multiple input arguments!");
            if(++i < argc) inputFileName = argv[i];
            else throw argument_exception("! Input File flag problem!");
            continue;
        }
        if(isStandardOutputFlag(argv[i])){
            if(standardOutput || outputFileName != "") throw argument_exception("! Multiple output arguments!");
            standardOutput = true;
            continue;
        }
        if(isOutputFileFlag(argv[i])){
            if(standardOutput || outputFileName != "") throw argument_exception("! Multiple output arguments!");
            if(++i < argc) outputFileName = argv[i];
            else throw argument_exception("! Output File flag problem!");
            continue;
        }
        else {
            std::string flag(argv[i]);
            throw argument_exception("!Unknown argument " + flag + " !");
            }
    }
    if(!standardOutput && outputFileName == "") throw argument_exception("! No output specified!");
    if(!standardInput && inputFileName == "") throw argument_exception("! No input specified!");
}

problem ArgumentParser::getProblem(){
    problem prob;
    std::istream* in;
    std::ifstream ifn;
    if(standardInput)
    in = &std::cin;
    else{
    ifn.open(inputFileName);
    if(!ifn.good()) throw argument_exception("! Nonexistent input file!");
    in = &ifn;
    }
    std::string line;
    std::getline(*in, line);
    std::stringstream ss(line);
    if(ss.eof())throw file_exception("! Input Error on first line!");
    ss >> prob.itemCount;
    if(ss.eof())throw file_exception("! Input Error on first line!");
    ss >> prob.backpackSize;
    if(!ss.eof()) throw file_exception("! Input Error on first line!");
    std::getline(*in, line);
    ss = std::stringstream(line);
    for(int i = 0; i < prob.itemCount; i++){
        int a;
        if(ss.eof()) throw file_exception("! Nothing more to read, expected more items in weights!");
        ss >> a;
        if(a < 1) throw file_exception("! Non-positive integer weight of an Item!");
        prob.itemWeights.push_back(a);
    }
    std::getline(*in, line);
    ss = std::stringstream(line);
    for(int i = 0; i < prob.itemCount; i++){
        int a;
        if(ss.eof()) throw file_exception("! Nothing more to read, expected more items in costs!");
        ss >> a;
        if(a < 1) throw file_exception("! Non-positive integer cost of an Item!");
        prob.itemCosts.push_back(a);
    }
    if(!ss.eof()) throw file_exception("! More then expected items declared on second line!");
    return prob;
}