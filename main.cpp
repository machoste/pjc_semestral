#include <iostream>
#include <thread>
#include <mutex>
#include "ArgumentParser.h"
#include "Exceptions.h"
#include "Solver.h"
#include <stdlib.h>

int main(int argc, char **argv){
  
    try{
        ArgumentParser parser{argc,argv};
        problem prob = parser.getProblem();
        Solver solver;
        solver.solve(prob, parser.getThreadCount());
        std::string outFile = parser.getOutputFile();
        std::ostream* out;
        std::ofstream ofn;
        if(outFile != ""){
            ofn.open(outFile);
            out = &ofn;
        }
        else{
            out = &std::cout;
        }
        solver.printSolution(*out);
    }
    catch(const help_exception& e){
        return 0;
    }
    catch(const my_exception& e){
        std::cerr << e.what() << std::endl;
        return -1;
    }
}