#include "Solver.h"

void Solver::solve(problem &prob, int threadCount){

#ifdef MEASURE
    auto start = std::chrono::high_resolution_clock::now();
#endif
    prob.table = std::vector<std::vector<int>> (prob.itemCount+1,std::vector<int>(prob.backpackSize+1));
    if(threadCount < 2){
        for(int itemID = 1; itemID < prob.itemCount+1; itemID++){
            int weight = prob.itemWeights[itemID-1];
            int cost = prob.itemCosts[itemID-1];
            prob.table[itemID] = prob.table[itemID-1];
            if(weight > prob.backpackSize){
                continue;
            }
            for(int sizeID = weight; sizeID <= prob.backpackSize; sizeID++){
                if(prob.table[itemID-1][sizeID - weight] + cost > prob.table[itemID-1][sizeID]){
                    prob.table[itemID][sizeID] = prob.table[itemID-1][sizeID-weight] + cost;
                }
            }
        }  
    }
    else{
        std::vector<std::thread> workers;
        std::condition_variable cv;
        std::condition_variable cvCopy;
        std::atomic<int> threadsDone;
        threadsDone = 0;
        std::mutex m;
        //Parallel
        for(int i = 0; i < threadCount; i++)
            workers.push_back(
                std::thread([i,threadCount,&prob,&cv,&m,&threadsDone,&cvCopy](){
                    for(int itemID = 1; itemID < prob.itemCount+1; itemID++){
                        int weight = prob.itemWeights[itemID-1];
                        int cost = prob.itemCosts[itemID-1];
                        std::unique_lock<std::mutex> lk(m);
                        threadsDone++;
                        //Synchronisation point for copying the last iteration into next
                        //so we don't have to copy it one by one
                        if(threadsDone < threadCount){
                            cv.wait(lk);
                        }
                        else{
                            prob.table[itemID] = prob.table[itemID-1];
                            cvCopy.notify_all();
                            threadsDone = 0;
                            cv.notify_all();
                        }
                        lk.unlock();
                        if(weight > prob.backpackSize){
                            continue;
                        }
                        int start = i * ((prob.backpackSize-weight)/threadCount) + weight;
                        int end = (i+1) * ((prob.backpackSize-weight)/threadCount) + weight;
                        if(i == threadCount-1) end = prob.backpackSize;
                        for(int sizeID = start; sizeID <= end; sizeID++){
                            if(weight > start) continue;
                            if(prob.table[itemID-1][sizeID - weight] + cost > prob.table[itemID-1][sizeID]){
                                prob.table[itemID][sizeID] = prob.table[itemID-1][sizeID-weight] + cost;
                            }
                        }
                        //Synchronisation point after each iteration
                        //We cannot have more running iterations at the same time, as they are iteratively dependent
                        lk.lock();
                        threadsDone++;
                        if(threadsDone < threadCount){
                            cv.wait(lk);
                        }
                        else{
                            threadsDone = 0;
                            cv.notify_all();
                        }
                    }
                })
            );
        std::for_each(workers.begin(), workers.end(), [](std::thread& t)
        {
            t.join();
        });
    }
    sol.totalCost = prob.table[prob.itemCount][prob.backpackSize];
    int w = prob.backpackSize;
    int tmp = sol.totalCost;
    for(int i = prob.itemCount; i > 0 && tmp > 0; i--){
        if(tmp == prob.table[i-1][w])continue;
        sol.itemsTaken.push_back(i);
        tmp -= prob.itemCosts[i-1];
        w -= prob.itemWeights[i-1];
    }
#ifdef MEASURE
    auto stop = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(stop-start);
    std::cout << "Time to complete:" << time.count() << " [ms]" << std::endl;
#endif
}
void Solver::printSolution(std::ostream &out){
    if(sol.itemsTaken.empty()){
        out << "No solution found!" << std::endl;
        return;
    }
    out << sol.totalCost << std::endl;
    for(int i = sol.itemsTaken.size()-1; i > 0; i--){
        out << sol.itemsTaken[i] << " ";
    }
    out << sol.itemsTaken[0] << std::endl;
}
