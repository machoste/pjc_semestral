#ifndef SEM_SOLVER_H
#define SEM_SOLVER_H

#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include <thread>
#include <algorithm>
#include <condition_variable>
#include <atomic>
#include "Exceptions.h"

#define MEASURE

struct problem{
    int backpackSize;
    int itemCount;
    std::vector<std::vector<int>> table;
    std::vector<int> itemWeights;
    std::vector<int> itemCosts; 
};

class Solver{
    struct Solution{
        std::vector<int> itemsTaken;
        int totalCost;
    };
    Solution sol;
public:
    void solve(problem &prob,int threadCount);
    void printSolution(std::ostream &out);
};


#endif